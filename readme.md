# How to use: 2019_08_16_Room_LEBONXavier   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.xavierlebon.room_lebonxavier":"https://gitlab.com/XavierLEBON/2019_08_16_Room_LEBONXavier.git",    
```    
--------------------------------------    
   
Feel free to support my work:    
Contact me if you need assistance:    
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.xavierlebon.room_lebonxavier",                              
  "displayName": "Room_LEBONXavier",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Simple Scene floor, door and monkey head",                         
  "keywords": ["Script","Tool","Room","Monkey"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    